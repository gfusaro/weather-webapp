export default class Geolocation {

  get SELECTORS() {
    return {
      geoButton: '#js-location'
    };
  }

  getUserLocation() {
    document.querySelector(this.SELECTORS.geoButton).addEventListener('click', function (e) {
      const geobtn = e.currentTarget;
      const _ctx = this;
      if ("geolocation" in navigator === false) return;

      function getCoords(position, e) {
        let geo = {}
        geo.lat = position.coords.latitude;
        geo.long = position.coords.longitude;
        _ctx.dispatchEvent(new CustomEvent("geolocation", {
          detail: { geo: geo },
          bubbles: true
        }));

        geobtn.disabled = true;
      }
      
      navigator.geolocation.getCurrentPosition(getCoords);
    });
  }

  init() {
    this.getUserLocation()
    console.log('INIT: Geolocation')
  }

}