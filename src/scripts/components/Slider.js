import SliderTemplate from '../templates/SliderTemplate';
import SlideTemplate from '../templates/SlideTemplate';
import DotTemplate from '../templates/DotTemplate';

export default class Slider {

  get SELECTORS() {
    return {
      sliderWrapper: '.slider-wrapper',
      slide: '.slider .item',
      dots: '.slider-dots',
      dot: '.slider-dots li',
      leftControl: '.slider-control.left',
      rightControl: '.slider-control.right'
    };
  }

  get CLASSES() {
    return {
      active: 'active',
      next: 'next',
      toLeft: 'to-left',
      toRight: 'to-right',
      fromLeft: 'from-left',
      fromRight: 'from-right'
    };
  }

  constructor(entries) {
    this.current = 0;
    this.entries = entries;
  }

  onCreateCarousel() {
    const slides = this.entries;
    let HTMLSlides = [];
    let HTMLDots = [];

    slides.map( (item, index) => {
      let cssClass = '';
      if(item) {
        index == 0 ? cssClass = 'active' : cssClass;
        HTMLSlides.push(SlideTemplate(item, cssClass))
        HTMLDots.push(DotTemplate(cssClass));
      }
    })
    const sliderTemplate = SliderTemplate(HTMLSlides.join(''), HTMLDots.join('') )
    document.querySelector(this.SELECTORS.sliderWrapper).innerHTML = sliderTemplate;
  } 

  changeCurrent(n) {
    this.current = (n + this.slides.length) % this.slides.length;
  }

  goToNextSlide(n) {
    this.hideSlide(this.CLASSES.toLeft);
    this.changeCurrent(n + 1);
    this.showSlide(this.CLASSES.fromRight);
  }

  goToPreviousSlide(n) {
    this.hideSlide(this.CLASSES.toRight);
    this.changeCurrent(n - 1);
    this.showSlide(this.CLASSES.fromLeft);
  }

  jumpToSlide(n) {
    if (n < this.current) {
      this.hideSlide(this.CLASSES.toRight);
      this.current = n;
      this.showSlide(this.CLASSES.fromLeft);
    } else {
      this.hideSlide(this.CLASSES.toLeft);
      this.current = n;
      this.showSlide(this.CLASSES.fromRight);
    }
  }

  hideSlide(direction) {
    const _ctx = this;
    this.slides[this.current].classList.add(direction);
    this.dots[this.current].classList.remove('active');
    this.slides[this.current].addEventListener('animationend', function () {
      this.classList.remove(_ctx.CLASSES.active, direction);
    });
  }

  showSlide(direction) {
    const _ctx = this;
    this.slides[this.current].classList.add(this.CLASSES.next, direction);
    this.dots[this.current].classList.add(this.CLASSES.active);
    this.slides[this.current].addEventListener('animationend', function () {
    this.classList.remove(_ctx.CLASSES.next, direction);
    this.classList.add(_ctx.CLASSES.active);
      _ctx.onChangeBackground();
    });
  }

  onChangeBackground() {
    const newUrl = document.querySelector('.slider .active .weather-card-wrapper').dataset.imageUrl;
    document.querySelector('.weather-image-container').style.backgroundImage = `url(${newUrl})`;
    // document.querySelector('.weather-image').src = newUrl;
  }

  bindEvents() {
    const _ctx = this;
    document.querySelector(_ctx.SELECTORS.leftControl).addEventListener('click', function () {
      _ctx.goToPreviousSlide(_ctx.current);
    });

    document.querySelector(_ctx.SELECTORS.rightControl).addEventListener('click', function () {
      _ctx.goToNextSlide(_ctx.current);
    });

    document.querySelector(this.SELECTORS.dots).addEventListener('click', function (e) {
      const target = [].slice.call(e.target.parentNode.children).indexOf(e.target);
      if (target !== _ctx.current && target < _ctx.dots.length) {
        _ctx.jumpToSlide(target);
      }
    });
  }

  init() {
    this.onCreateCarousel();
    this.bindEvents();
    this.slides = document.querySelectorAll(this.SELECTORS.slide);
    this.dots = document.querySelectorAll(this.SELECTORS.dot);
    this.onChangeBackground();
    console.log('INIT: Slider ')
  }

}