import WeatherCardModel from '../models/WeatherCardModel'
import DayForecastModel from '../models/DayForecastModel'
import WeatherCardTemplate from '../templates/WeatherCardTemplate';
import DayForecastTemplate from '../templates/DayForecastTemplate';
import apiConfig from '../../config/apiConfig';
import arrayChunker from '../utils/ArrayChunker'
import Slider from './Slider';

export default class WeatherCard {

  /**
  *
  * INFO:
  * openweathermap.org does not provides forecast api for several city as for weather
  * and free API forecasts are limited to 5 days including today, thus 4
  * just to match weather + forecast and for dev speed purposes we're calling one api at a time, then we pack and consume data
  * this can be improved by purchasing a license for these APIs
  */

  get SELECTORS() {
    return {
      section: '.widget',
      weather: '.weather-card',
      forecast: '.forecast-list',
      pointer: '.pointer',
      weatherImg: '.weather-image'
    };
  }

  constructor() {
    this.WeatherCardTemplate = WeatherCardTemplate,
    this.apiConfig = apiConfig
  }

  cityUrls(localCoords) {
    const config = this.apiConfig;
    let cities = config.cities; // It's freezed!!
    !localCoords ? cities = config.cities : cities = [localCoords, ...cities ];
    let urls = [];
    cities.map(city => {
      const weatherUrl = `${config.baseUrl}/data/2.5/${config.type.weather}?lat=${city.lat}&lon=${city.long}&appid=${this.apiConfig.apiKey}&units=${this.apiConfig.units}`;
      const forecastUrl = `${config.baseUrl}/data/2.5/${config.type.forecast}?lat=${city.lat}&lon=${city.long}&appid=${this.apiConfig.apiKey}&units=${this.apiConfig.units}`;
      urls.push(weatherUrl);
      urls.push(forecastUrl)
    })
    return urls
  }

  async fetchWeatherData(localCoords) {
    const urls = this.cityUrls(localCoords);
    try {
      async function fetchAll() {
        const data = await Promise.all(
          urls.map(url => fetch(url).then((response) => response.json()))
        );
        return data
      }
      fetchAll().then(data => {
        const blob = arrayChunker(data, 2);
        this.render(blob)
      })
    } catch (error) {
      console.log(error)
      throw (error)
    }
  }

  onComposeWeatherImageUrl(urls, code) {
    const genericCode = code.split('').slice(0, -1).join(''); //codes are for day and night, but we don't need this specificity
    let imgUrl = '';
    urls.map(x => {
      if (x.includes(genericCode)) {
        imgUrl = x.trim();
      }
    })
    return imgUrl;
  }

  onComposeWeatherCard(data) {
    const rawFData = data[1];
    const rawWData = data[0];
    const tomorrow = new Date().setHours(24, 0, 0, 0);
    const cleanWData = new WeatherCardModel(rawWData);
    const weatherCode = rawWData.weather[0].icon;
    const bgUrls = document.querySelector(this.SELECTORS.section).getAttribute('data-urls').split(',');
    const bgUrl = this.onComposeWeatherImageUrl(bgUrls, weatherCode);
    const windDirection = cleanWData.windDirection;
    const card = WeatherCardTemplate(cleanWData, bgUrl, windDirection, this.onComposeForecastStripe(rawFData, tomorrow));
    
    return card
  }

  onComposeForecastStripe(rawFData, tomorrow) {
    let HTMLforecast = [];
    let filtered = [];
    rawFData.list.map((item, index) => {
      if ((item.dt * 1000) > tomorrow && index % 8 === 0) { // today "now time" forecasts!
        filtered.push(item);
      }
    })
    filtered.map(item => {
      if (item) {
        const cleanFData = new DayForecastModel(item);
        HTMLforecast.push(DayForecastTemplate(cleanFData));
      }
    })

    return HTMLforecast.join('')
  }

  render(data) {
    let HTMLSlides = [];
    data.map((item, index) => {
      HTMLSlides.push(this.onComposeWeatherCard(data[index]));
    })
    new Slider(HTMLSlides).init();
  }

  init() {
    this.fetchWeatherData();
    document.addEventListener('geolocation', () => {
      this.fetchWeatherData(event.detail.geo);
    });
    console.log('INIT: WeatherCard ')
  }

}


