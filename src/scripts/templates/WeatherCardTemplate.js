const WeatherCardTemplate = (wdata, bgUrl, windDirection, forecastItem) => {  
  return (
    `
    <div class="weather-card-wrapper" data-image-url="${bgUrl}">
      <div class="backdrop"></div>
      <div class="current-weather">
        <h2 class="city-name">${wdata.name}</h2>
        <h4 class="city-weather">${wdata.weather}</h4>
        <p class="city-temp">${wdata.temp}°</p>
        <div class="extra-data">
          <div class="data-box">
            <p class="temp-range">${wdata.tempMin}° / ${wdata.tempMax}°</p>
          </div>
          <div class="data-box">
            <div class="city-wind">
              <div class="compass">
                <div class="pointer" style="transform: rotate(${windDirection}deg)"></div>
              </div>
              <span class="speed">${wdata.windSpeed} m/s</span>
            </div>
          </div>
        </div>
      </div>
      <ul class="forecast-list">${forecastItem}</ul>
    </div>
    `
  )
}

export default WeatherCardTemplate