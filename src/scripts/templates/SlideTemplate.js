const SlideTemplate = (content, cssClass) => {
  return (
    `
      <div class="item ${cssClass}">
       ${content}
      </div>
    `
  )
}

export default SlideTemplate