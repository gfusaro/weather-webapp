const DayForecastTemplate = (fdata) => {
  return (
    `
      <li class="forecast-item">
        <span class="day">${fdata.day}</span>
        <img src='http://openweathermap.org/img/wn/${fdata.icon}@2x.png' alt="weather"/>
        <span class="temp">${fdata.tempMin}° / ${fdata.tempMax}°</span>
      </li>
    `
  )
}

export default DayForecastTemplate



