const DotTemplate = (cssClass) => {
  return (
    `
      <li class="dot ${cssClass}"></li>
    `
  )
}

export default DotTemplate
