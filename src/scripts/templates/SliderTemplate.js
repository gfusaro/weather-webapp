const SliderTemplate = (slides, dots) => {
  return (
    `
      <button class="slider-control left" aria-label="previous">
        <div class="arrow left icon-chevron_left"></div>
      </button>
        <div class="slider">
          ${slides}
        </div>
      <button class="slider-control right" aria-label="next">
        <div class="arrow right icon-chevron_right"></div>
      </button>
      <ol class="slider-dots">
        ${dots}
      </ol>
    `
  )
}

export default SliderTemplate
