import '../stylesheets/style.scss';
import './components/Geolocation';

import WeatherCard from './components/WeatherCard';
import Geolocation from './components/Geolocation';

/**
 * Init classes
 */
new WeatherCard().init();
new Geolocation().init();
