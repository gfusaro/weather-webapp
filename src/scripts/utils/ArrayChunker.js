const arrayChunker = (inputArray, chunkSize) => inputArray.reduce((resultArray, item, index) => {
  const chunkIndex = Math.floor(index / chunkSize)
  if (!resultArray[chunkIndex]) {
    resultArray[chunkIndex] = []
  }
  resultArray[chunkIndex].push(item)
  return resultArray
}, [])

export default arrayChunker
