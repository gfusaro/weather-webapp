export default class WeatherCardModel {
  constructor(data) {
    this.name = data.name
    this.weather = data.weather[0].description
    this.temp = parseInt(data.main.temp)
    this.tempMin = parseInt(data.main.temp_min)
    this.tempMax = parseInt(data.main.temp_max)
    this.windDirection = parseInt(data.wind.deg)
    this.windSpeed = data.wind.speed
  }
}
