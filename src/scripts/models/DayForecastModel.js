export default class DayForecastModel {
  constructor(data) {    
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    this.day = days[new Date(data.dt * 1000).getUTCDay()]
    this.icon = data.weather[0].icon
    this.tempMin = parseInt(data.main.temp_min)
    this.tempMax = parseInt(data.main.temp_max)
  }
}
