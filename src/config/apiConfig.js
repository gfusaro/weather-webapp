/**
 * INFO:
 * Call current weather data for several cities: http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric
 * NOTE: A single ID counts as a one API call! So, the above example is treated as a 3 API calls.
 */

const apiConfig = Object.freeze({
  baseUrl: 'https://api.openweathermap.org',
  apiKey: '8356cb638c3b1748c952c9c88e1a9743',
  fallBackApiKey: '21bbb29f245c4c6162f52effe685b014',
  type: {
    weather: 'weather',
    forecast: 'forecast'
  },
  units: 'metric',
  cities: [
    {
      city: 'New_York',
      lat: 40.7166638,
      long: - 74.0
    },
    {
      city: 'London',
      lat: 51.50853,
      long: -0.12574
    },
    {
      city: 'San_Francisco',
      lat: 37.77493,
      long: -122.41942
    },
    {
      city: 'Dubai',
      lat: 25.2697,
      long: 55.3095
    },
    {
      city: 'Rio_de_Janeiro',
      lat: -22.9032315871 ,
      long: -43.1729427749
    },
    {
      city: 'Paris',
      lat: 48.85341,
      long: 2.3488
    }
  ]
})

export default apiConfig;




 